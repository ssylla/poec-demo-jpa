DROP TABLE IF EXISTS LIVRE;
CREATE TABLE LIVRE
(
    ID     INT(10)      NOT NULL PRIMARY KEY,
    TITRE  VARCHAR(255) NOT NULL,
    AUTEUR VARCHAR(50)  NOT NULL
);
INSERT INTO LIVRE
VALUES (1, 'Vingt mille lieues sous les mers', 'Jules Verne');
INSERT INTO LIVRE
VALUES (2, 'Germinal', 'Emile Zola');
INSERT INTO LIVRE
VALUES (3, 'Guerre et paix', 'Léon Tolstoï');
INSERT INTO LIVRE
VALUES (4, 'Apprendre à parler aux animaux', 'Gaston Pouet');
INSERT INTO LIVRE
VALUES (5, '1001 recettes de Cuisine', 'Jean-Pierre Coffe');