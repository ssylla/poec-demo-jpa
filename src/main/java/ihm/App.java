package ihm;

import domain.Company;
import domain.Person;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class App {
	
	public static void main( String[] args ) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory( "pu-demo" );
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		//
		// //Lecture
		// Person person = em.find(Person.class, 1L);
		// System.out.println(person);
		// person.setFullName( "Séga S" );
		//
		// //Création
		// Person person1 = new Person( "Corentin V", "cov@cov" );
		// em.persist( person1 );
		//
		// //Remove
		// Person person2 = em.find( Person.class, 6L );
		// if (null != person2) {
		// 	em.remove( person2 );
		// }
		
		Company company = new Company( "Sebsy. Corp 2" );
		Person contact = new Person( "Sega. S Mod", "ssylla2" );
		contact.setCompany( company );
		
		em.persist( company );
		
		em.getTransaction().commit();
		emf.close();
	}
}
