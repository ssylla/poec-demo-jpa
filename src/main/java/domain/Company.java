package domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.HashSet;

@Entity
public class Company implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	
	@OneToMany(mappedBy = "company", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	Set<Person> contacts = new HashSet<>();
	
	public Company() {
	}
	
	public Company( String name ) {
		this.name = name;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId( Long id ) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName( String name ) {
		this.name = name;
	}
	
	public Set<Person> getContacts() {
		return contacts;
	}
	
	public void setContacts( Set<Person> contacts ) {
		this.contacts = contacts;
	}
	
	public void addContact(Person contact) {
		if (null != contact) {
			contact.setCompany( this );
		}
	}
}
