package domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "t_person")
public class Person implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "nom_complet")
	private String fullName;
	private String twitter;
	@Transient
	private int age;
	
	@ManyToOne
	// @JoinColumn(name = "id_soc")
	private Company company;
	
	public Person() {
	}
	
	public Person( String fullName, String twitter ) {
		this.fullName = fullName;
		this.twitter = twitter;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId( Long id ) {
		this.id = id;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public void setFullName( String fullName ) {
		this.fullName = fullName;
	}
	
	public String getTwitter() {
		return twitter;
	}
	
	public void setTwitter( String twitter ) {
		this.twitter = twitter;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge( int age ) {
		this.age = age;
	}
	
	public Company getCompany() {
		return company;
	}
	
	public void setCompany( Company company ) {
		if (null != this.company) {
			this.company.getContacts().remove( this );
		}
		this.company = company;
		if (null != this.company) {
			this.company.getContacts().add( this );
		}
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Person{" );
		sb.append( "id=" ).append( id );
		sb.append( ", fullName='" ).append( fullName ).append( '\'' );
		sb.append( ", twitter='" ).append( twitter ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
